<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire de choix selectionné</title>
    
    <?php include 'shared/head.php';?>

</head>

<script>
var tousMesTextes = [
  "texte 1"
  "texte 2"
  "texte 3"
  "texte 4"
  "texte 5"
  "texte 6"
]
</script>

<body>
<div class="container">
<div class=" p-5 text-center">
<h1>Composez votre parcours</h1>
</div>
</div>


<div class="row" ml=1OOpx>
<div class="container">
<form>

    

<div class="container">
  <div class="col-12 col-6">
  <div class="card">
  <div class="card-header text-center">
  Formulaire :
  </div>
  <div class="card-body">
    <h5 class="card-title">Choisissez votre parcours :</h5>
    <form>
  <div class="row">
    <div class="col">
      <input type="Date" class="form-control" placeholder="Date">
    </div>
    <div class="col">
      <input type="time" class="form-control" placeholder="Heure">
    </div>
    
  </div>


<div class="row">
    <div class="col-8-lg-3">
    <div class="card-group">
  <div class="card">
    <img src="..." class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">BOUTIQUES</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
      <div class="form-check">
        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
      </div>
    </div>
  </div>
  <div class="card">
    <img src="..." class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">CINEMAS</h5>
      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
      <div class="form-check">
        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
      </div>
    </div>
  </div>
  <div class="card">
    <img src="..." class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">BARS</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
      <div class="form-check">
        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
      </div>
    </div>
  </div>
  <div class="card">
    <img src="..." class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">RESTAURANTS</h5>
      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
      <div class="form-check">
        <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
      </div>
    </div>
  </div>
</div>
    </div>
</div>


  <div class="form-group">
    <label for="exampleFormControlSelect1">Example select</label>
    <select class="form-control" id="exampleFormControlSelect1">
      <option></option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>

      <button class=" mt-3 btn btn-success" href=""> Valider</button>

</form>
  </div>
</div>
  </div>
</div>

</body>
</html>
