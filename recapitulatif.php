<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Récapitulatif</title>
    
    <?php 
   include 'shared/head.php';
   ?>

</head>

<body>
<script src="https://use.typekit.net/bkt6ydm.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<header class="example-header">
    <h1 class="text-center">Simple Responsive Timeline</h1>
</header><div class="container">
  <ul>
    <li>
      <span></span>
      <div>
        <div class="title">Votre sélection de parcours</div>
        <div class="info">programme</div>
        <div class="type">Prensetation</div>
      </div>
      
      <span class="number">
        <span>10:00</span>
        <span>12:00</span>
      </span>
    </li>
    <li>
      <div>
        <span></span>
        <div class="title">Codify</div>
        <div class="info">Let's make coolest things in javascript</div>
        <div class="type">Prensetation</div>
      </div>
      <span class="number">
        <span>13:00</span>
        <span>14:00</span>
      </span>
    </li>
    <li>
      <div>
        <span></span>
        <div class="title">Codify</div>
        <div class="info">Let's make coolest things in css</div>
        <div class="type">Review</div>
      </div>
      <span class="number">
        <span>15:00</span>
        <span>17:45</span>
      </span>
    </li>
  </ul>
</div>
</body>
</html>